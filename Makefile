.PHONY: behat

# ================================================================================
# PROJECT VARS - YOU CAN UPDATE
# ================================================================================
step=/////////////////////
project=fft-selenium
projectCompose=fft-selenium
console_path=app/console
makefiles_path=.zol/zol-common/makefile
cache_log_prefix=var
# ================================================================================
# COMMON VARS - YOU MUST NOT UPDATE
# ================================================================================

ifeq "$(wildcard $(makefiles_path) )" "$(makefiles_path)"
    include $(makefiles_path)/assets.mk
	include $(makefiles_path)/common.mk
    include $(makefiles_path)/composer.mk
    include $(makefiles_path)/docker.mk
    include $(makefiles_path)/hooks.mk
    include $(makefiles_path)/proxy.mk
    include $(makefiles_path)/symfony.mk
endif

# ================================================================================
# CUSTOM
# ================================================================================

$(call check_defined, USER_CMD)

install: remove clean-app build start composer-install js-install gulp cache-clear

zol-common:
	@rm -rf .zol
	@mkdir -p .zol
	@cd .zol && git clone --branch v3.1.5 git@bitbucket.org:zol/zol-common.git

behat:
	@echo "$(step) Running functional tests $(step)"
	@$(compose) run --rm web /bin/bash -c "$(USER_CMD) bin/behat"