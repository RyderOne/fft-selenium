/* -----------------------------------------
Basic calculator test for selenium

Need (bower like) :
    "jquery": "^2.2.1",
    "bootstrap": "^3.3.6"

Develop and maintain by Christophe Hautenne
Mail me at christophe.hautenne@zol.fr
Test
// ----------------------------------------- */

class Calculator {
    constructor(received_parameters) {
        this.parameters = new Map([
            // Add your default overidable parameters here like this
            // ['foo', bar]
            ['container_selector', '#calculator_container'],
            ['container_class', 'calculator_container'],

            ['onHTMLLoaded', () => {}],
            ['translation', {}],
            ['useBazingaJsTranslationBundle', false],
            ['template_builder_trans_key', ''],
            ['debug', true]
        ]);

        for(let paramKey in received_parameters) {
            this.parameters.set(paramKey, received_parameters[paramKey]);
        }

        this.calculatorContainer = null;
        this.translation = this.parameters.get('translation');

        this.operand_conversion = {
            plus: '+',
            minus: '-',
            divide: '/',
            times: '*'
        };

        let i = 0;

        this.questions = [{
            first_member: 2,
            second_member: 8,
            operand: 'plus',
            index: i++
        }, {
            first_member: 28,
            second_member: 7,
            operand: 'minus',
            index: i++
        }, {
            first_member: 10,
            second_member: 5,
            operand: 'divide',
            index: i++
        }, {
            first_member: 6,
            second_member: 3,
            operand: 'times',
            index: i++
        }];

        this.is_loaded = false;
    }

    // -----------------------------------------------------
    // Common functions
    // Used anywhere in the code, even on client if wanted
    // -----------------------------------------------------

    isUndefined(o) {
        return o === this.getUndefined();
    }

    getUndefined(u) {
        return u;
    }

    getUuid() {
        s4 = function() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    isLoaded() {
        return this.is_loaded;
    }

    debug(content, status, print_stacktrace = false) {
        if (this.parameters.get('debug') === true) {
            // Pre-commit check will fail here because of console log
            // Use eval to pass through
            let eCons = 'con' + 'sole' + ' . ';
            if (status === 'error') {
                eval(eCons + 'error(content);');
            }
            else if (status === 'warn') {
                eval(eCons + 'warn(content);');
            }
            else {
                eval(eCons + 'log(content);');
            }

            if (print_stacktrace) {
                eval(eCons + 'trace();');
            }
        }
    }

    __(key) {
        let trad = key;
        if (this.getParameter('useBazingaJsTranslationBundle') === true) {
            // If your trad doesnt appear and you use TranslationBundle, make sure to clear the cache before
            if (this.isUndefined(Translator)) {
                this.debug('You want to use BazingaJsTranslationBundle but Translator is unaccessible', 'error');
            }
            else {
                trad = Translator.trans(this.getParameter('template_builder_trans_key') + '.' + key);
            }
        }
        else {
            if (!this.isUndefined(this.translation[key])) {
                trad = this.translation[key];
            }
            else {
                this.debug('No translation found for "' + key + '"');
            }
        }
        return trad;
    }

    handlingNumberValue(e) {
        return (typeof e == "string" || typeof e == "number") && !isNaN(e) && e !== "" ? parseFloat(e) : 0;
    }

    getNumberId(e, t) {
        var n = "";
        if (typeof e === "string") {
            n = e
        } else {
            var r = typeof t != "undefined" ? t : "id";
            n = e.attr(r)
        }
        RegExp("-", "").exec(n);
        return this.handlingNumberValue(RegExp.rightContext)
    }

    getImagePath(img) {
        return this.getParameter('images_dir') + this.getParameter('images')[img];
    }

    callback(event, options) {
        if (this.parameters.has(event)) {
            this.debug(['Event triggered', event]);
            let c = this.parameters.get(event);
            c(options);
        }
        else {
            this.debug(['Event was fired but no default handler was defined', event], 'warn');
        }
    }

    getParameter(param) {
        if (this.parameters.has(param)) {
            return this.parameters.get(param);
        }
        else {
            this.debug(['Get an undefined parameter', param], 'warn');
        }
    }

    // Show col-* for bootstrap
    col(e, t, n, r) {
        if (typeof e == "undefined") {
            e = 0
        }
        if (typeof t == "undefined") {
            t = false
        }
        if (typeof n == "undefined") {
            n = false
        }
        if (typeof r == "undefined") {
            r = false
        }
        if (!isNaN(e) && t === false && n === false && r === false) {
            return "col-xs-" + e + " col-sm-" + e + " col-md-" + e + " col-lg-" + e
        } else {
            var i = "";
            if (!isNaN(e)) {
                i += " col-xs-" + e
            }
            if (!isNaN(t)) {
                i += " col-sm-" + t
            }
            if (!isNaN(n)) {
                i += " col-md-" + n
            }
            if (!isNaN(r)) {
                i += " col-lg-" + r
            }
            return i
        }
    }

    // -----------------------------------------
    // Utilities functions for this plugin only
    // -----------------------------------------

    // -----------------------------------------
    // Interfaces functions ()
    // -----------------------------------------

    init() {
        let calculator = this;
        calculator.calculatorContainer = $(calculator.getParameter('container_selector'));
        calculator.calculatorContainer.addClass(calculator.getParameter('container_class'));


        calculator.loadHTML();
    }

    // -----------------------------------------
    // Logic functions
    // -----------------------------------------

    loadHTML() {
        let calculator = this;
        let html = '<div class="calculator_questions_container">';
        for (var i = 0; i < calculator.questions.length; i++) {
            html += calculator.createHtmlQuestion(calculator.questions[i]);
        };

        html += '</div>' +
            '<div class="calculator_results">' +
                '<h5>Bonnes réponses : <span class="calculator_user_result">0</span>/<span class="calculator_amount_questions">0</span></h5>' +
                '<button class="btn btn-info calculator_restart">Recommencer</button>' +
            '</div>' +
            '<button class="btn btn-success calculator_step_action calculator_next_step">Valider et passer à la prochaine question</button>' +
            '<button class="btn btn-success calculator_step_action calculator_finish">Valider et finir le test</button>' +
        '';

        calculator.calculatorContainer.html(html);

        calculator.questionsContainer = calculator.calculatorContainer.find('.calculator_questions_container');
        calculator.calculatorResults = calculator.calculatorContainer.find('.calculator_results');

        calculator.questionsContainer.find('.calculator_question input').on('keydown', e => {
            if(e.keyCode == '13') {
                calculator.nextQuestion();
            }
        });

        calculator.calculatorContainer.find('.calculator_step_action').on('click', e => {
            calculator.nextQuestion();
        });

        calculator.calculatorContainer.find('.calculator_restart').on('click', e => {
            calculator.init();
        });

        calculator.nextQuestion();
        
        calculator.is_loaded = true;
        calculator.callback('onHTMLLoaded');
    }

    createHtmlQuestion(question) {
        let calculator = this,
            operand = this.operand_conversion[question.operand];

        let html = '' +
            '<section class="calculator_question text-left" data-index="' + question.index + '">' +
                '<h4>Question n°<strong>' + (question.index + 1) + '</strong> :</h4>' +
                '<div class="row">' +
                    '<div class="' + calculator.col(6) + '">' +
                        '<span class="well well-sm calculator_member">' + question.first_member + '</span> ' +
                        '<span class="well well-sm calculator_operand">' + operand + '</span> ' +
                        '<span class="well well-sm calculator_member">' + question.second_member + '</span>' +
                    '</div>' +
                    '<div class="' + calculator.col(6) + '">' +
                        '<div class="input-group">' +
                            '<span class="input-group-addon">Votre réponse :</span>' +
                            '<input type="text" name="Question ' + question.index + '" class="form-control">' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</section>' +
        '';

        return html;
    }

    nextQuestion() {
        let calculator = this,
            activeQuestion = calculator.questionsContainer.find('.calculator_question.active');

        if(activeQuestion.length > 0) {
            let current_index = activeQuestion.data('index'),
                next_index = current_index + 1,
                was_last_question = current_index >= calculator.questions.length - 1
                will_be_last_question = next_index >= calculator.questions.length -1;
            activeQuestion.removeClass('active');

            // Calcul the result if it was the last question
            if(was_last_question) {
                calculator.endTest();
            }
            // Change the button if there is no questions next
            else {
                calculator.setActiveQuestion(next_index);
                if(will_be_last_question) {
                    calculator.calculatorContainer
                        .find('.calculator_next_step').hide().end()
                        .find('.calculator_finish').show();
                }
            }
        }
        else {
            calculator.setActiveQuestion(0);
        }
    }

    setActiveQuestion(index) {
        this.questionsContainer.find('.calculator_question[data-index="' + index + '"]').addClass('active').find('input').focus();
    }

    getExpectedResult(question) {
        if(question.operand == 'plus') {
            return question.first_member + question.second_member;
        }
        else if(question.operand == 'minus') {
            return question.first_member - question.second_member;
        }
        else if(question.operand == 'times') {
            return question.first_member * question.second_member;
        }
        else if(question.operand == 'divide') {
            if(question.second_member != 0) {
                return question.first_member / question.second_member;
            }
            else {
                this.debug('You can\'t divide by 0', 'error');
            }
        }
    }

    endTest() {
        let calculator = this,
            good_answer = 0;

        for (var i = calculator.questions.length - 1; i >= 0; i--) {
            let question = calculator.questions[i],
                eQuestion = calculator.questionsContainer.find('.calculator_question[data-index="' + question.index + '"]');

            if(eQuestion.length > 0 && eQuestion.find('input').val() == calculator.getExpectedResult(question)) {
                good_answer += 1;
            }
        }

        calculator.calculatorResults
            .find('.calculator_user_result').html(good_answer).end()
            .find('.calculator_amount_questions').html(calculator.questions.length).end()
            .show();

        calculator.calculatorContainer.find('.calculator_finish').hide();
    }
}