#!/usr/bin/env bash
set -o errexit

__DIR__="$(cd "$(dirname "${0}")"; echo $(pwd))"
__BASE__="$(basename "${0}")"
__FILE__="${__DIR__}/${__BASE__}"

# Give permissions to read and write in workdir for users root and www-data
setfacl -R -m u:www-data:rwx -m u:root:rwx /var/www
setfacl -dR -m u:www-data:rwx -m u:root:rwx /var/www

# Function to update the fpm configuration to make the service environment variables available
function setEnvironmentVariable() {

    local key=$1
    local value=$2

    # Force conf to use ENV from container
    # E.G.: Set SYMFONY_ENV="$SYMFONY_ENV"
    value="\"\$$key\""

    # Check whether variable already exists
    if grep -q $key /etc/php5/fpm/pool.d/www.conf; then
        # Reset variable
        sed -i "s/^env\[$key.*/env[$key] = $value/g" /etc/php5/fpm/pool.d/www.conf
    else
        # Add variable
        echo "Set $key=$value in www.conf"
        echo "env[$key] = \"$value\"" >> /etc/php5/fpm/pool.d/www.conf
    fi
}

# Grep for Symfony variables
for _curVar in `env | grep ^SYMFONY_ | awk -F = '{print $1}'`;do
    setEnvironmentVariable ${_curVar} ${!_curVar}
done

main () {



    if [ "$PROJECT_ENV" == "dev" ]
    then
        cat /tmp/php.ini.dev >> /etc/php5/fpm/php.ini
    else
        cat /tmp/php.ini >> /etc/php5/fpm/php.ini
    fi

    if [ "$PROJECT_ENV" == "dev" ]
    then
        cat /tmp/php.ini.dev >> /etc/php5/cli/php.ini
    else
        cat /tmp/php.ini >> /etc/php5/cli/php.ini
    fi

    php5-fpm
    /usr/sbin/nginx
}

main