<?php
namespace AppBundle\Tests\Contexts;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Mink\Element\NodeElement;

class FeatureContext extends MinkContext
{
    /**
     * @Then /^(?:|I )press button "(?P<element>(?:[^"]|\\")*)"$/
     */
    public function pressOnElement($element)
    {
        $element = $this->getSession()->getPage()->find('css', $element);
        $this->assertElementExists($element);

        $element->press();
    }

    /**
     * @Then /^(?:|I )fill field "(?P<field>(?:[^"]|\\")*)" with value "(?P<value>(?:[^"]|\\")*)"$/
     */
    public function fillFieldWithValue($field, $value)
    {
        $field = $this->getSession()->getPage()->findField($field);
        $this->assertElementExists($field);

        $field->setValue($value);
    }

    protected function assertElementExists(NodeElement $nodeElement = null)
    {
        if (null === $nodeElement) {
            throw new \Exception('The element is not found');
        }
    }

    /**
     * Use this function when you want to debug your page
     *
     * @Then I wait infinitly
     */
    public function iWaitInfinitly()
    {
        // Sleep 1 hour
        sleep(1 * 60 * 60);

        // $tries = 0;
        // while ($tries < (1 * 60 * 60)) {
            
        //     $result = $this->getSession()->evaluateScript("return window.document.title;");
        //     $tries += 1;
        // }
    }
}
