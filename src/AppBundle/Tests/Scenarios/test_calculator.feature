@testing_js
Feature: Check calculator results
    In order to test my knowledge
    As an user
    I need to fill fields and get calculator test result

    @javascript
    Scenario: Going on homepage
        When I go to "/"
        And I press button ".startTestButton"
        And I fill field "Question 0" with value "10"
        And I press button ".calculator_next_step"
        And I fill field "Question 1" with value "21"
        And I press button ".calculator_next_step"
        And I fill field "Question 2" with value "2"
        And I press button ".calculator_next_step"
        And I fill field "Question 3" with value "10"
        And I press button ".calculator_finish"

        Then I should see "3" in the ".calculator_user_result" element

        Then I press button ".calculator_restart"

        And I fill field "Question 0" with value "10"
        And I press button ".calculator_next_step"
        And I fill field "Question 1" with value "21"
        And I press button ".calculator_next_step"
        And I fill field "Question 2" with value "2"
        And I press button ".calculator_next_step"
        And I fill field "Question 3" with value "18"
        And I press button ".calculator_finish"

        Then I should see "4" in the ".calculator_user_result" element